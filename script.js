let $list;
let $myInput;
let $addButton;
let $closePopUpBtn;
let $acceptPopUp;
let $popUpInput;
let $spanToChange;
let elementId;


const initialList = [];

//Start code and preparing events and elements --------------------------------------

function main() {
    prepareDOMElements();
    prepareDOMEvents();
    prepareInitialList();
    getTodos();
}

function prepareDOMElements() {
    $list = document.getElementById('list');
    $myInput = document.getElementById('myInput');
    $addButton = document.getElementById('addBtn');
    $closePopUpBtn = document.getElementById('closePopUp'); 
    $popUpInput = document.getElementById('popUpInput');
    $acceptPopUp = document.getElementById('popUpAddButton');  
    $modal = document.getElementById('popup'); 
}

function prepareDOMEvents() {
    $addButton.addEventListener('click', addButtonClickHandler);
    $closePopUpBtn.addEventListener('click', closePopUpFunc);
    $acceptPopUp.addEventListener('click', popUpChangeTitle);
}

//Preparing initial list ------------------------------------------------------------

function prepareInitialList() {
    initialList.forEach(todo => {
        addNewElementToList(todo);
    })
}

// Add Todo ----------------------------------------------------------------------

async function getTodos() {

    todos = await axios.get('http://195.181.210.249:3000/todo/')
        .then(
            success_get => {
                console.log("git");
                console.log(success_get);
                success_get.data.filter(iteratorek => iteratorek.author == 'K.O.').forEach(iteratorek => {
                    
                    addNewElementToList(iteratorek);
                
            });
            },
            fail_get => {
                console.log("fail");
                console.log(fail_get);
            }
    );
}

async function addNewTodo(title) {
    await axios.post('http://195.181.210.249:3000/todo/', {
    title: title,
    author: 'K.O.',
}).then((response) => {
    console.log(response);
  }, (error) => {
    console.log(error);
  })
  .then (refreshList);
}

function addNewElementToList(title) {
    newElement = createElement(title);
    $list.appendChild(newElement);
}

function createElement (title) {
    const newElement = document.createElement('li');
    newElement.classList.add('listElement');
    newElement.id = title.id;
    newElement.author = title.author;

    const newSpan = document.createElement('span')
    newElement.appendChild(newSpan);
    newSpan.innerHTML = title.title;

    const newDiv = document.createElement('div');
    newDiv.classList.add('buttonsContainer');
    newElement.appendChild(newDiv);

    const deleteButton = document.createElement("BUTTON");
    deleteButton.className = "delete button";
    deleteButton.innerHTML = "DELETE";
    deleteButton.addEventListener('click', deleteTodo);
    deleteButton.addEventListener('click', removeElement);
    newDiv.appendChild(deleteButton);

    const doneButton = document.createElement("BUTTON");
    doneButton.className = "done button";
    doneButton.innerHTML = "DONE";
    doneButton.addEventListener('click', markAsDone);
    newDiv.appendChild(doneButton);

    const editButton = document.createElement("BUTTON");
    editButton.className = "edit button";
    editButton.innerHTML = "EDIT";
    editButton.addEventListener('click', openPopUp);
    newDiv.appendChild(editButton);
    
    return newElement;
}

function addButtonClickHandler() {
    addNewTodo($myInput.value);
}

//Delete Todo -----------------------------------------------------------------------

async function deleteTodo() {
    await axios.delete('http://195.181.210.249:3000/todo/' + this.parentElement.parentElement.id)
    .then((response) => {
        console.log(response);
      }, (error) => {
        console.log(error);
      });
}

function removeElement() {
    this.parentElement.parentElement.remove();
}

function refreshList() {
    let elel = $list.children;
   while(elel[0]) {
       elel[0].remove();
   };
   getTodos();
}

//Change todo ---------------------------------------------------------------------------

async function changeTitleElement(newTitle) {
    await axios.put('http://195.181.210.249:3000/todo/' + elementId.id, {
        title: newTitle,
        author: 'K.O.',
    })
    .then((response) => {
        console.log(response);
      }, (error) => {
        console.log(error);
      })
      .then (refreshList);
    closePopUpFunc();
}

function popUpChangeTitle() {
    changeTitleElement($popUpInput.value); 
}

function markAsDone() {
    if (this.parentElement.parentElement.classList.contains("doneMark")) {
        this.parentElement.parentElement.classList.remove("doneMark");	
    }
    else {
        this.parentElement.parentElement.classList.add("doneMark");
    }
}

// Modal function -------------------------------------------------------------------

function openPopUp() {
    elementId = this.parentElement.parentElement;
    $modal.style.display = 'flex';
}

function closePopUpFunc() {
    $modal.style.display = 'none';
}

document.addEventListener('DOMContentLoaded', main);